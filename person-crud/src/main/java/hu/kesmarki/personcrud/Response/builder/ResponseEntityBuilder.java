package hu.kesmarki.personcrud.Response.builder;

import hu.kesmarki.personcrud.Response.model.ResponseModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseEntityBuilder {

    public static ResponseEntity<?> getResponseEntity(ResponseModel responseModel) {
        if (responseModel.getStatus() == HttpStatus.OK) {
            if(responseModel.getElement() != null) {
                return ResponseEntity.ok().body(responseModel.getElement());
            }

            if(responseModel.getElements() != null) {
                return ResponseEntity.ok().body(responseModel.getElements());
            }

            if(responseModel.getMessage() != null) {
                return ResponseEntity.ok().body(responseModel.getMessage());
            }
        }
        return ResponseEntity.status(responseModel.getStatus()).body(responseModel.getMessage());
    }
}
