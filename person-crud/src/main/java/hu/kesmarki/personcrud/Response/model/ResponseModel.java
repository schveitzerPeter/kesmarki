package hu.kesmarki.personcrud.Response.model;

import hu.kesmarki.personcrud.Common.AbstractDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

@NoArgsConstructor
@Data
public class ResponseModel {
    private HttpStatus status;
    private AbstractDTO element;
    private List<AbstractDTO> elements;
    private String message;
}
