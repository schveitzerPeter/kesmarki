package hu.kesmarki.personcrud.Address.model;


import hu.kesmarki.personcrud.Common.AbstractDTO;
import hu.kesmarki.personcrud.Contact.model.Contact;
import hu.kesmarki.personcrud.Person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "address")
public class Address extends AbstractDTO {


    @Id
    @Column(name = "address_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long addressId;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private String zip;

    @Column(name = "address")
    private String address;


    @ManyToOne
    @JoinColumn(name="person_id", nullable=false)
    private Person person;

    @OneToMany(mappedBy="address", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Contact> contact;
}
