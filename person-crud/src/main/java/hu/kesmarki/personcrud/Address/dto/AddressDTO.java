package hu.kesmarki.personcrud.Address.dto;


import hu.kesmarki.personcrud.Common.AbstractDTO;
import hu.kesmarki.personcrud.Contact.dto.ContactDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressDTO extends AbstractDTO {
    private String city;
    private String state;
    private String zip;
    private String address;
    private List<ContactDTO> contact;
}
