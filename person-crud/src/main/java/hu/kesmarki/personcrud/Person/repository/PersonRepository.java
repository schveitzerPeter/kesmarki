package hu.kesmarki.personcrud.Person.repository;

import hu.kesmarki.personcrud.Person.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT p FROM Person p where p.personId = :personId")
    Optional<Person> getPersonByPersonId(Long personId);
}
