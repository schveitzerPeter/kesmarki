package hu.kesmarki.personcrud.Person.service;

import hu.kesmarki.personcrud.Response.model.ResponseModel;
import org.springframework.stereotype.Service;

@Service
public interface PersonService {
    ResponseModel getPersonByPersonId(Long personId);
    ResponseModel deletePersonByPersonId(Long personId);

    ResponseModel createPerson();

    ResponseModel updatePersonByPersonId(Long personId);
}
