package hu.kesmarki.personcrud.Person;

import hu.kesmarki.personcrud.Person.service.PersonService;
import hu.kesmarki.personcrud.Response.builder.ResponseEntityBuilder;
import hu.kesmarki.personcrud.Response.model.ResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @GetMapping("/{personId}")
    public ResponseEntity<?> getPersonByPersonId( @Valid @PathVariable("personId") Long personId ){
        ResponseModel responseModel = personService.getPersonByPersonId(personId);
        return ResponseEntityBuilder.getResponseEntity(responseModel);
    }

    @DeleteMapping("/{personId}")
    public ResponseEntity<?> deletePersonByPersonId( @Valid @PathVariable("personId") Long personId ){
        ResponseModel responseModel = personService.deletePersonByPersonId(personId);
        return ResponseEntityBuilder.getResponseEntity(responseModel);
    }
}
