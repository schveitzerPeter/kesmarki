package hu.kesmarki.personcrud.Person.dto;

import hu.kesmarki.personcrud.Address.dto.AddressDTO;
import hu.kesmarki.personcrud.Address.model.Address;
import hu.kesmarki.personcrud.Common.AbstractDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonDTO extends AbstractDTO {
    private String firstName;
    private String lastName;
    private List<AddressDTO> address;
}
