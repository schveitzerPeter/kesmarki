package hu.kesmarki.personcrud.Person.service;

import hu.kesmarki.personcrud.Address.dto.AddressDTO;
import hu.kesmarki.personcrud.Contact.dto.ContactDTO;
import hu.kesmarki.personcrud.Person.dto.PersonDTO;
import hu.kesmarki.personcrud.Person.model.Person;
import hu.kesmarki.personcrud.Person.repository.PersonRepository;
import hu.kesmarki.personcrud.Response.model.ResponseModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;

import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService{
    private final PersonRepository personRepository;
    @Override
    public ResponseModel getPersonByPersonId(Long personId) {
        ResponseModel responseModel = new ResponseModel();
        Person optionalPerson = personRepository.getPersonByPersonId( personId ).orElse(null);

        if( optionalPerson != null ){
            //This should be done with a mapper
            PersonDTO personDTO= new PersonDTO();
            personDTO.setFirstName(optionalPerson.getFirstName());
            personDTO.setLastName(optionalPerson.getLastName());
            personDTO.setAddress(
                optionalPerson.getAddress().stream().map( address -> {
                    AddressDTO addressDTO = new AddressDTO();
                    addressDTO.setAddress(address.getAddress());
                    addressDTO.setCity(address.getCity());
                    addressDTO.setZip(address.getZip());
                    addressDTO.setState(address.getState());
                    addressDTO.setContact(
                        address.getContact().stream().map(contact -> {
                            ContactDTO contactDTO = new ContactDTO();
                            contactDTO.setType(contact.getType());
                            contactDTO.setValue(contact.getValue());
                            return contactDTO;
                        }).collect(Collectors.toList())
                    );
                    return addressDTO;
                }).collect(Collectors.toList())
            );

            responseModel.setElement(personDTO);
            responseModel.setStatus(HttpStatus.OK);
        } else {
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setMessage("Person with id: " + personId + " was not found in the database!");
        }
        return responseModel;
    }

    @Override
    public ResponseModel deletePersonByPersonId(Long personId) {
        return null;
    }

    @Override
    public ResponseModel createPerson() {
        return null;
    }

    @Override
    public ResponseModel updatePersonByPersonId(Long personId) {
        return null;
    }
}
