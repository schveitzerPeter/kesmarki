package hu.kesmarki.personcrud.Contact.model;

import hu.kesmarki.personcrud.Address.model.Address;
import hu.kesmarki.personcrud.Person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "contact")
public class Contact {

    @Id
    @Column(name = "contact_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long contactId;

    @Column(name = "type")
    private String type;

    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name="address_id", nullable=false)
    private Address address;
}
