package hu.kesmarki.personcrud.Contact.dto;

import hu.kesmarki.personcrud.Common.AbstractDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ContactDTO  extends AbstractDTO {
    private String type;
    private String value;
}
