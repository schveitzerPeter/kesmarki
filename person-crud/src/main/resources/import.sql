insert into person values( 'Hideg','Richárd');
insert into person values( 'Zaid','Lutz');
insert into person values( 'Kyan','Clements');
insert into person values( 'Alec','Solomon');
insert into person values( 'Isabel','Bautista');
insert into person values( 'Trevon','Kemp');
insert into person values( 'Leila','Chan');
insert into person values( 'Kristian','Carr5');


insert into address values( 'Marty-éri evezőspálya út 5', 'Szeged', 'Csongrad', '6710', (select person_id from person where first_name = 'Hideg') );
insert into address values( 'Back Bernát utca 6', 'Szeged', 'Csongrad', '6791', (select person_id from person where first_name = 'Hideg') );

insert into contact values('tel','(06 62) 553 500',(select address_id from address WHERE zip = '6791'));
insert into contact values('fax','(06 62) 554 542',(select address_id from address WHERE zip = '6791'));
insert into contact values('web','http://www.hansa-kontakt.hu',(select address_id from address WHERE zip = '6791'));